module bitbucket.org/nadilas/subscription

go 1.14

require (
	github.com/kr/pretty v0.2.1
	github.com/reugn/go-streams v0.5.2 // indirect
	github.com/stretchr/testify v1.6.1
	go.temporal.io/sdk v1.1.0
)
