package domain

type (
	BizEvent struct {
		Type string
		Data string
	}
	UnsubscribeMessage struct {
		MessageType string
	}
	SubscribeMessage struct {
		MessageType string
	}
)
