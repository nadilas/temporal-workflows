package subscription_test

import (
	"errors"
	"testing"
	"time"

	subscription "bitbucket.org/nadilas/subscription"
	"bitbucket.org/nadilas/subscription/domain"
	"github.com/stretchr/testify/suite"
	"go.temporal.io/sdk/testsuite"
	"go.temporal.io/sdk/workflow"
)

type UserSubscriptionTestSuite struct {
	suite.Suite
	testsuite.WorkflowTestSuite

	env *testsuite.TestWorkflowEnvironment
}

func TestUserSubscriptionTestSuite(t *testing.T) {
	suite.Run(t, &UserSubscriptionTestSuite{})
}

func (s *UserSubscriptionTestSuite) SetupTest() {
	s.env = s.NewTestWorkflowEnvironment()
}

func (s *UserSubscriptionTestSuite) BeforeTest(suiteName, testName string) {

}

func (s *UserSubscriptionTestSuite) AfterTest(suiteName, testName string) {
	s.env.AssertExpectations(s.T())
}

func (s *UserSubscriptionTestSuite) Test_RemovingLastEmailExitsWorkflow() {
	s.env.RegisterDelayedCallback(func() {
		s.env.SignalWorkflow(subscription.UnsubscribeSignal, domain.UnsubscribeMessage{MessageType: "eventtype"})
	}, time.Minute*5)

	s.env.ExecuteWorkflow(subscription.SubscriptionWorkflow, subscription.Details{Subscriber: "test@user.com"})

	s.True(s.env.IsWorkflowCompleted())
	s.NoError(s.env.GetWorkflowError())
}

func (s *UserSubscriptionTestSuite) Test_Adds2Removes2SubsExitsWorkflow() {
	s.env.RegisterDelayedCallback(func() {
		s.env.SignalWorkflow(subscription.SubscribeSignal, domain.SubscribeMessage{MessageType: "sub1"})
		s.env.SignalWorkflow(subscription.SubscribeSignal, domain.SubscribeMessage{MessageType: "sub2"})
		s.env.SignalWorkflow(subscription.UnsubscribeSignal, domain.UnsubscribeMessage{MessageType: "sub1"})
		s.env.SignalWorkflow(subscription.UnsubscribeSignal, domain.UnsubscribeMessage{MessageType: "sub2"})
	}, time.Minute*5)

	s.env.ExecuteWorkflow(subscription.SubscriptionWorkflow, subscription.Details{Subscriber: "test@user.com"})

	s.True(s.env.IsWorkflowCompleted())
	s.NoError(s.env.GetWorkflowError())
}

func (s *UserSubscriptionTestSuite) Test_ReceiveBusinessEventAndExitWorkflow() {
	s.env.RegisterDelayedCallback(func() {
		s.env.SignalWorkflow(subscription.BusinessEventSignal, domain.BizEvent{Type: "biz-event-type1"})
		s.env.SignalWorkflow(subscription.UnsubscribeSignal, domain.UnsubscribeMessage{MessageType: "sub2"})
	}, time.Minute*5)

	s.env.ExecuteWorkflow(subscription.SubscriptionWorkflow, subscription.Details{Subscriber: "test@user.com"})

	s.True(s.env.IsWorkflowCompleted())
	s.NoError(s.env.GetWorkflowError())
}

func (s *UserSubscriptionTestSuite) Test_RehydratingWorkflow_After_ContinueAsNewFrequency() {
	s.env.RegisterDelayedCallback(func() {
		for i := 0; i < 150; i++ {
			s.env.SignalWorkflow(subscription.BusinessEventSignal, domain.BizEvent{Type: "biz-event-type1"})
		}
	}, time.Minute*5)

	s.env.ExecuteWorkflow(subscription.SubscriptionWorkflow, subscription.Details{Subscriber: "test@user.com"})

	s.True(s.env.IsWorkflowCompleted())
	var err *workflow.ContinueAsNewError
	s.True(errors.As(s.env.GetWorkflowError(), &err))
}

func (s *UserSubscriptionTestSuite) Test_QuerySubsAfter2ActiveSubs_Succeed() {
	s.env.RegisterDelayedCallback(func() {
		s.env.SignalWorkflow(subscription.SubscribeSignal, domain.SubscribeMessage{MessageType: "sub1"})
		s.env.SignalWorkflow(subscription.SubscribeSignal, domain.SubscribeMessage{MessageType: "sub2"})
	}, time.Minute*5)

	s.env.RegisterDelayedCallback(func() {
		val, err := s.env.QueryWorkflow("subscriptions")
		s.NoError(err)
		var subs []string
		s.NoError(val.Get(&subs))
		s.Equal(2, len(subs), "does not have 2 subs")
		s.Contains(subs, "sub1", "sub1 missing")
		s.Contains(subs, "sub2", "sub2 missing")
	}, time.Minute*8)

	// exit
	s.env.RegisterDelayedCallback(func() {
		s.env.SignalWorkflow(subscription.UnsubscribeSignal, domain.UnsubscribeMessage{MessageType: "sub1"})
		s.env.SignalWorkflow(subscription.UnsubscribeSignal, domain.UnsubscribeMessage{MessageType: "sub2"})
	}, time.Minute*15)

	s.env.ExecuteWorkflow(subscription.SubscriptionWorkflow, subscription.Details{Subscriber: "test@user.com"})

	s.True(s.env.IsWorkflowCompleted())
	s.NoError(s.env.GetWorkflowError())
}

func (s *UserSubscriptionTestSuite) Test_QuerySubs_Should_Return_2ActiveSubs_AfterRehydration() {
	s.env.RegisterDelayedCallback(func() {
		s.env.SignalWorkflow(subscription.SubscribeSignal, domain.SubscribeMessage{MessageType: "sub1"})
		s.env.SignalWorkflow(subscription.SubscribeSignal, domain.SubscribeMessage{MessageType: "sub2"})
		for i := 0; i < 150; i++ {
			s.env.SignalWorkflow(subscription.BusinessEventSignal, domain.BizEvent{Type: "biz-event-type1"})
		}
	}, time.Minute*5)

	s.env.RegisterDelayedCallback(func() {
		val, err := s.env.QueryWorkflow("subscriptions")
		s.NoError(err)
		var subs []string
		s.NoError(val.Get(&subs))
		s.Equal(2, len(subs), "does not have 2 subs")
		s.Contains(subs, "sub1", "sub1 missing")
		s.Contains(subs, "sub2", "sub2 missing")
	}, time.Minute*8)

	// exit
	s.env.RegisterDelayedCallback(func() {
		s.env.SignalWorkflow(subscription.UnsubscribeSignal, domain.UnsubscribeMessage{MessageType: "sub1"})
		s.env.SignalWorkflow(subscription.UnsubscribeSignal, domain.UnsubscribeMessage{MessageType: "sub2"})
	}, time.Minute*15)

	s.env.ExecuteWorkflow(subscription.SubscriptionWorkflow, subscription.Details{Subscriber: "test@user.com"})

}
