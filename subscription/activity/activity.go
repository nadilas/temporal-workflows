package activity

import (
	"context"

	"bitbucket.org/nadilas/subscription/domain"
)

type (
	UserSubscription struct{}
)

func (s *UserSubscription) Notify(ctx context.Context, event domain.BizEvent) error {
	panic("not implemented yet")
}
