package main

import (
	"bitbucket.org/nadilas/subscription"
	"bitbucket.org/nadilas/subscription/activity"
	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/worker"
)

func main() {
	cli, err := client.NewClient(client.Options{})
	if err != nil {
		panic(err)
	}
	w := worker.New(cli, "queue", worker.Options{})
	w.RegisterWorkflow(subscription.SubscriptionWorkflow)
	userSub := &activity.UserSubscription{}
	w.RegisterActivity(userSub)

	err = w.Run(worker.InterruptCh())
	if err != nil {
		panic(err)
	}
}
