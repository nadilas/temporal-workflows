package subscription

import (
	"bitbucket.org/nadilas/subscription/activity"
	"bitbucket.org/nadilas/subscription/domain"
	"github.com/kr/pretty"
	"go.temporal.io/sdk/workflow"
)

const (
	UnsubscribeSignal   = "UNSUBSCRIBE_SIGNAL"
	SubscribeSignal     = "SUBSCRIBE_SIGNAL"
	BusinessEventSignal = "BUSINESS_EVENT_SIGNAL"
)

type (
	Details struct {
		Subscriber string
	}
)

const continueAsNewFrequency = 150

func SubscriptionWorkflow(ctx workflow.Context, details Details) error {
	logger := workflow.GetLogger(ctx)
	var subscription *activity.UserSubscription

	childCtx, cancelHandler := workflow.WithCancel(ctx)
	var subscriptions []string
	findSubIdx := func(sub string) int {
		for idx, s := range subscriptions {
			if s == sub {
				return idx
			}
		}
		return -1
	}

	// setup query handlers
	err := workflow.SetQueryHandler(ctx, "subscriptions", func(input []byte) ([]string, error) {
		return subscriptions, nil
	})
	if err != nil {
		logger.Info("SetQueryHandler failed: " + err.Error())
		return err
	}

	shutdown := false
	for i := 0; i < continueAsNewFrequency; i++ {
		selector := workflow.NewSelector(childCtx)

		// exit condition
		selector.AddReceive(childCtx.Done(), func(c workflow.ReceiveChannel, more bool) {
			c.Receive(ctx, nil)
			shutdown = true
		})

		// Business events
		selector.AddReceive(workflow.GetSignalChannel(childCtx, BusinessEventSignal), func(c workflow.ReceiveChannel, more bool) {
			var bizEvent *domain.BizEvent
			c.Receive(childCtx, &bizEvent)
			logger.Info("BizEvent received", pretty.Sprint(bizEvent))
			workflow.Go(ctx, func(ctx workflow.Context) {
				workflow.ExecuteActivity(ctx, subscription.Notify, bizEvent)
			})
		})

		// Signal Actions
		selector.AddReceive(workflow.GetSignalChannel(childCtx, UnsubscribeSignal), func(c workflow.ReceiveChannel, more bool) {
			var unsubMsg *domain.UnsubscribeMessage
			c.Receive(childCtx, &unsubMsg)
			logger.Info("Unsubscribe signal received", pretty.Sprint(unsubMsg))
			idx := findSubIdx(unsubMsg.MessageType)
			if idx >= 0 {
				subscriptions = append(subscriptions[0:idx], subscriptions[idx+1:]...)
			}

			exitCondition := len(subscriptions) < 1
			if exitCondition {
				// cleanup workflow, no subscriptions are left
				cancelHandler()
			}
		})
		selector.AddReceive(workflow.GetSignalChannel(childCtx, SubscribeSignal), func(c workflow.ReceiveChannel, more bool) {
			var subMsg *domain.SubscribeMessage
			c.Receive(childCtx, &subMsg)
			logger.Info("Subscribe signal received", pretty.Sprint(subMsg))
			subscriptions = append(subscriptions, subMsg.MessageType)
		})

		selector.Select(childCtx)
		if shutdown {
			// workflow is finished for good.
			return nil
		}
	}

	logger.Info("User subscription rehydrating....", "email", details.Subscriber)
	return workflow.NewContinueAsNewError(ctx, SubscriptionWorkflow, details)
}
