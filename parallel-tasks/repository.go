package parallel

import "context"

//go:generate mockgen -source=repository.go -package=mocks -destination=mocks/repository.go

type WorkflowTemplate interface {
	UUID() string
}

type Task interface {
	UUID() string
}

type WorkflowManager interface {
	GetWorkflow() Workflow
	GetTasks() []Task
}

type Publish interface {
	PublishJSON(ctx context.Context, topic string, data interface{})
}
