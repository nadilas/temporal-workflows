package parallel_test

import (
	"errors"
	"testing"
	"time"

	"bitbucket.org/nadilas/parallel"
	"bitbucket.org/nadilas/parallel/mocks"
	. "bitbucket.org/nadilas/testutils"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"go.temporal.io/sdk/temporal"
	"go.temporal.io/sdk/workflow"
)

func TestParallelTestSuite(t *testing.T) {
	suite.Run(t, &ParallelTestSuite{})
}

type ParallelTestSuite struct {
	BDTemporalTestSuite
	mockCtrl  *gomock.Controller
	publisher *mocks.MockPublish
}

func (s *ParallelTestSuite) BeforeTest(suiteName, testName string) {
	s.mockCtrl = gomock.NewController(s.T())
	s.publisher = mocks.NewMockPublish(s.mockCtrl)
}

func (s *ParallelTestSuite) AfterTest(suiteName, testName string) {
	s.Env.AssertExpectations(s.T())
}

func (s *ParallelTestSuite) Test_Basics() {
	var wf *parallel.Workflow
	var eventData interface{}
	havingEventData := func(eData *interface{}, ed interface{}) func(s **BDTemporalTestSuite) {
		return func(s **BDTemporalTestSuite) {
			*eData = ed
		}
	}
	aWorkflowWith3Steps := func(workflow **parallel.Workflow) func(s **BDTemporalTestSuite) {
		return func(s **BDTemporalTestSuite) {
			(*s).Env.RegisterWorkflow(parallel.StepForApproval)
			*workflow = &parallel.Workflow{
				Uuid:      "1",
				Type:      "DFlow",
				Status:    100,
				Assignees: nil,
				Steps: []*parallel.Step{
					{Uuid: "1", Position: 100},
					{Uuid: "2", Position: 200},
					{Uuid: "3", Position: 300},
				},
			}
		}
	}
	aNilWorkflow := func(workflow **parallel.Workflow) func(s **BDTemporalTestSuite) {
		return func(s **BDTemporalTestSuite) {
			*workflow = nil
			(*s).Env.RegisterWorkflow(parallel.StepForApproval)
		}
	}
	s.Scenario("Workflow created with nil request",
		s.Given(aNilWorkflow(&wf)),
		s.And(havingEventData(&eventData, wf)),
		s.And(s.expectNotification("workflow.drafted", &eventData)),
		s.When(iStartWorkflow(&wf)),
		s.Then(func(suite **BDTemporalTestSuite) {
			st := *suite
			st.True(st.Env.IsWorkflowCompleted())
			err := st.Env.GetWorkflowError()
			st.NotNil(err)
			var appError *temporal.ApplicationError
			st.True(errors.As(err, &appError))
			st.EqualError(appError.Unwrap(), parallel.NotInitializedError.Error())
		}),
	)
	s.Scenario("Workflow drafted, wait for requestor submit",
		s.Given(aWorkflowWith3Steps(&wf)),
		s.And(havingEventData(&eventData, wf)),
		s.And(s.expectNotification("workflow.drafted", &eventData)),
		// wait for requestor submissing task
		s.And(expectTaskWorkflow(&wf, "1")),
		s.When(iStartWorkflow(&wf)),
		s.Then(workflowTimeout()),
	)
	s.Scenario("Workflow drafted, requestor submits in 5 Min",
		s.Given(aWorkflowWith3Steps(&wf)),
		s.And(havingEventData(&eventData, wf)),
		s.And(s.expectNotification("workflow.drafted", &eventData)),
		s.And(expectRequestorSubmitIn5Min(&wf, "1")),
		s.And(s.expectNotification("workflow.created", &eventData)),
		// TODO s.And(s.expectEmail(&wf, "workflow.created")),
		s.When(iStartWorkflow(&wf)),
		s.Then(workflowContinueAsNew()),
	)
	s.Scenario("Workflow drafted, requestor submits in 5 Min",
		s.Given(aWorkflowWith3Steps(&wf)),
		s.And(havingEventData(&eventData, wf)),
		s.And(s.expectNotification("workflow.drafted", &eventData)),
		s.And(expectRequestorSubmitIn5Min(&wf, "1")),
		s.And(s.expectNotification("workflow.created", &eventData)),
		// TODO s.And(s.expectEmail(&wf, "workflow.created")),
		s.When(iStartWorkflow(&wf)),
		s.Then(workflowTimeout()),
	)
}

func (s *ParallelTestSuite) Test_Reset_To_Templates() {
	// 1. reset to template same task group - no emails whatsoever
	// 2. reset to template new earlier task - revoke later task group, assign new task group via email
	// 3. reset to template same task group but one step deleted - revoke deleted step via email
}

func expectTaskWorkflow(wf **parallel.Workflow, stepIds ...string) func(s **BDTemporalTestSuite) {
	return func(s **BDTemporalTestSuite) {
		st := *s
		for _, step := range (*wf).Steps {
			if StrArrContains(stepIds, step.Uuid) {
				st.Env.OnWorkflow(parallel.StepForApproval, mock.Anything, step).Return(func(ctx workflow.Context, stp *parallel.Step) (bool, error) {
					sel := workflow.NewSelector(ctx)
					sel.Select(ctx)
					return true, nil
				})
			}
		}
	}
}

func expectRequestorSubmitIn5Min(wf **parallel.Workflow, stepIds ...string) func(s **BDTemporalTestSuite) {
	return func(s **BDTemporalTestSuite) {
		st := *s
		for _, step := range (*wf).Steps {
			if StrArrContains(stepIds, step.Uuid) {
				st.Env.OnWorkflow(parallel.StepForApproval, mock.Anything, step).Return(func(ctx workflow.Context, stp *parallel.Step) (bool, error) {
					sel := workflow.NewSelector(ctx)
					// simulate incoming signal in 5 Min
					sel.AddFuture(workflow.NewTimer(ctx, time.Minute*5), func(f workflow.Future) {

					})
					sel.Select(ctx)
					return true, nil
				})
			}
		}
	}
}

func (s *ParallelTestSuite) expectValidationTask(wf **parallel.Workflow) func(suite **BDTemporalTestSuite) {
	return func(suite **BDTemporalTestSuite) {
		s.publisher.EXPECT().PublishJSON(gomock.Any(), "workflow.created", *wf)
		// TODO also check for e-mail to segment admins
	}
}

func (s *ParallelTestSuite) expectNotification(event string, data *interface{}) func(suite **BDTemporalTestSuite) {
	return func(suite **BDTemporalTestSuite) {
		s.publisher.EXPECT().PublishJSON(gomock.Any(), event, *data)
	}
}

func iStartWorkflow(wf **parallel.Workflow) func(suite **BDTemporalTestSuite) {
	return func(suite **BDTemporalTestSuite) {
		st := *suite
		st.Env.ExecuteWorkflow(parallel.Parallel, *wf)
	}
}

func workflowNotFinished() func(s **BDTemporalTestSuite) {
	return func(s **BDTemporalTestSuite) {
		st := *s
		st.False(st.Env.IsWorkflowCompleted())
		st.Nil(st.Env.GetWorkflowError())
	}
}

func workflowFinished() func(s **BDTemporalTestSuite) {
	return func(s **BDTemporalTestSuite) {
		st := *s
		st.True(st.Env.IsWorkflowCompleted())
		st.Nil(st.Env.GetWorkflowError())
	}
}

func workflowTimeout() func(s **BDTemporalTestSuite) {
	return func(s **BDTemporalTestSuite) {
		st := *s
		st.True(st.Env.IsWorkflowCompleted())
		err := st.Env.GetWorkflowError()
		st.NotNil(err)
		var tout *temporal.TimeoutError
		st.True(errors.As(err, &tout))
		st.Equal(tout.TimeoutType().String(), "ScheduleToClose")
	}
}

func workflowContinueAsNew() func(s **BDTemporalTestSuite) {
	return func(s **BDTemporalTestSuite) {
		st := *s
		st.True(st.Env.IsWorkflowCompleted())
		err := st.Env.GetWorkflowError()
		st.NotNil(err)
		var casn *temporal.WorkflowExecutionError
		st.True(errors.As(err, &casn))
		st.Equal(casn.Unwrap().Error(), "continue as new")
	}
}
