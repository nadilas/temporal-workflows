module bitbucket.org/nadilas/parallel

go 1.14

require (
	bitbucket.org/nadilas/testutils v0.0.0-00010101000000-000000000000
	github.com/golang/mock v1.4.4
	github.com/stretchr/testify v1.6.1
	go.temporal.io/sdk v1.2.0
)

replace bitbucket.org/nadilas/testutils => ../testutils
