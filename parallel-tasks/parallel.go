package parallel

import (
	"errors"
	"time"

	"go.temporal.io/sdk/temporal"
	"go.temporal.io/sdk/workflow"
)

var (
	NotInitializedError = errors.New("workflow data not initialized")
)

func Parallel(ctx workflow.Context, workflowData *Workflow) error {
	logger := workflow.GetLogger(ctx)
	state := &WorkflowSM{}
	if workflowData == nil {
		return temporal.NewNonRetryableApplicationError("dropping invalid workflow", "NotInitializedError", NotInitializedError)
	}
	sel := workflow.NewSelector(ctx)

	// main loop
	i := 0
	for i < 80000 && !state.shutdown {
		submitted, requestorStep := isRequestAlreadySubmitted(workflowData)
		// todo extract into findNextTaskGroup()
		if !submitted {
			parallelGroupContext, cancelHandler := workflow.WithCancel(ctx)

			// should we receive a reopen signal from an earlier task-group -- run cancelHandler()
			submitChildWf := assignAndWaitForRequestorToSubmit(parallelGroupContext, workflowData, requestorStep)
			sel.AddFuture(submitChildWf, func(f workflow.Future) {
				var approved bool
				if err := f.Get(ctx, &approved); err != nil {
					state.shutdown = true
					state.retryErr = err
				}
				if approved {
					// re-read data
					requestorStep.Approved = true
					state.shutdown = true
					state.retryErr = workflow.NewContinueAsNewError(ctx, Parallel, workflowData) // restart with modified data assign next steps
				}
			})
		}
		sel.Select(ctx)
		i++
	}

	// early exit retry check
	if state.retryErr != nil {
		return state.retryErr
	}

	// TODO workflow completed, send notification
	logger.Info("Workflow completed", "workflowId", workflowData)
	return nil
}

func assignAndWaitForRequestorToSubmit(ctx workflow.Context, workflowData *Workflow, requestorStep *Step) workflow.ChildWorkflowFuture {
	rp := &temporal.RetryPolicy{
		InitialInterval:    time.Second,
		BackoffCoefficient: 2.0,
		MaximumInterval:    time.Minute,
	}
	childCtx := workflow.WithChildOptions(ctx, workflow.ChildWorkflowOptions{
		WorkflowID:  requestorStep.Uuid,
		RetryPolicy: rp,
		Memo: map[string]interface{}{
			"RequestId": workflowData.Uuid,
		},
	})
	submitChildWf := workflow.ExecuteChildWorkflow(childCtx, StepForApproval, requestorStep)
	return submitChildWf
}

func isRequestAlreadySubmitted(workflowData *Workflow) (bool, *Step) {
	var requestorStep *Step
	submitted := false
	if workflowData.Steps != nil {
		requestorStep = workflowData.Steps[0]
		submitted = requestorStep.Approved
	}
	return submitted, requestorStep
}

type ParallelizedRequest struct {
}

func StepForApproval(ctx workflow.Context, stepData *Step) (bool, error) {
	logger := workflow.GetLogger(ctx)
	defer func() {
		if ctx.Err() == workflow.ErrCanceled {
			// todo send revoked email, task no longer active
		} else if peerCompleteNotificationActive {
			// todo send peer-complete email, task no longer active
		}
	}()
	sel := workflow.NewSelector(ctx)
	sel.Select(ctx) // wait for user input
	logger.Info("Assigned step approved", "stepId", stepData)
	return true, nil
}

type WorkflowSM struct {
	shutdown bool // todo replace with grpcsync.NewEvent() ?
	retryErr error
}
