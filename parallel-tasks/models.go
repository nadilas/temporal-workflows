package parallel

type Workflow struct {
	Uuid      string
	Type      string
	Status    int
	Assignees []*MdrsAssignee
	Steps     []*Step
}

type Step struct {
	Uuid     string
	Position int
	Approved bool
}

type MdrsAssignee struct {
	Uuid              string
	StepId            string
	AllowedPerformers []string
}
